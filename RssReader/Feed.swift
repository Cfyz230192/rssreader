//
//  Feed.swift
//  RssReader
//
//  Created by Alexandr Ponomarev on 12/02/2017.
//  Copyright © 2017 Alex. All rights reserved.
//

import Foundation

struct Feed {
    var title: String!
    var link: String!
    var description: String!

    init(title: String, link: String, description: String) {
        self.link = link
        self.title = title
        self.description = description
    }
}
