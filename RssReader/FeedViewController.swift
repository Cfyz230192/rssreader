//
//  FeedViewController.swift
//  RssReader
//
//  Created by Alexandr Ponomarev on 11/02/2017.
//  Copyright © 2017 Alex. All rights reserved.
//

import UIKit

class FeedViewController: UIViewController {

    @IBOutlet private weak var webView: UIWebView?

    var feed: Feed?

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = feed?.title

        webView?.scalesPageToFit = true
        webView?.loadRequest(URLRequest(url: URL(string: (feed?.link)!)!))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
}
