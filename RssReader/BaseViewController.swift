//
//  BaseViewController.swift
//  RssReader
//
//  Created by Alexandr Ponomarev on 11/02/2017.
//  Copyright © 2017 Alex. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {

        var nibName = nibNameOrNil

        if nibName == nil {
            let fullClassName = NSStringFromClass(type(of: self))
            let nameComponents = fullClassName.components(separatedBy: ".")
            nibName = nameComponents.last
        }

        super.init(nibName: nibName, bundle: nibBundleOrNil)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
