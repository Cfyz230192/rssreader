//
//  NetworkManager.swift
//  RssReader
//
//  Created by Alexandr Ponomarev on 12/02/2017.
//  Copyright © 2017 Alex. All rights reserved.
//

import Foundation
import SWXMLHash

class NetworkManager {

    static let sharedInstance = NetworkManager()

    func getFeedsForURL(feedResource: [FeedResource], completionHandler: @escaping ([FeedResource : [Feed]]) -> ()) {
        var feedsDictionary = [FeedResource : [Feed]]()

        for feedResource in feedResource {
            var feeds = [Feed]()
            let request = NSMutableURLRequest(url: URL(string: feedResource.url)!)
            let session = URLSession.shared
            request.httpMethod = "GET"

            let task = session.dataTask(with: request as URLRequest) {
                (data, response, error) in

                if let data = data {
                    func pars() {
                        let xml = SWXMLHash.parse(data)

                        for elem in xml["rss"]["channel"]["item"] {
                            let feed = Feed.init(title: (elem["title"].element?.text)!,
                                                 link: (elem["link"].element?.text)!,
                                                 description: (elem["description"].element?.text)!)
                            feeds.append(feed)
                        }

                        feedsDictionary[feedResource] = feeds
                        completionHandler(feedsDictionary)
                    }

                    DispatchQueue.main.async(execute: pars)
                }
            }
            task.resume()
        }
    }
}
