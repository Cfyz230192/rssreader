//
//  FeedResourceListViewController.swift
//  RssReader
//
//  Created by Alexandr Ponomarev on 13/02/2017.
//  Copyright © 2017 Alex. All rights reserved.
//

import UIKit

class FeedResourceListViewController: UIViewController {

    @IBOutlet private weak var tableView: UITableView!

    var feedResources = [FeedResource]() {
        didSet {
            tableView.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Resources"
        feedResources = DataBaseManager.sharedInstance.getFeedResource()

        tableView.registerReusable(reusableClass: FeedResourceTableViewCell.self)

        setupNavigationItems()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension FeedResourceListViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feedResources.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FeedResourceTableViewCell.reuseIdentifier, for: indexPath)

        (cell as! FeedResourceTableViewCell).feedResource = feedResources[indexPath.row]
        (cell as! FeedResourceTableViewCell).delegate = self

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let feedResource = feedResources[indexPath.row]

        let alert = UIAlertController(title: "RSS", message: "Edit your RSS", preferredStyle: .alert)

        alert.addTextField { (textField) in
            textField.placeholder = "Name"
            textField.text = feedResource.name
        }

        alert.addTextField { (textField) in
            textField.placeholder = "Link"
            textField.text = feedResource.url
        }

        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
            let nameTextField = alert.textFields![0]
            let linkTextField = alert.textFields![1]

            var name = ""
            var link = ""

            if nameTextField.text?.characters.count == 0 {
                name = "No Name"
            }
            else {
                name = (nameTextField.text)!
            }

            if linkTextField.text?.characters.count == 0 {
                return
            }
            else {
                link = (linkTextField.text)!
            }

            DataBaseManager.sharedInstance.editFeedResource(id: feedResource.id, name: name, link: link, isFavorite: feedResource.isFavorite)
            self.feedResources = DataBaseManager.sharedInstance.getFeedResource()
        }))

        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action: UIAlertAction!) in
        }))

        self.present(alert, animated: true, completion: nil)
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        let feedResource = feedResources[indexPath.row]

        DataBaseManager.sharedInstance.deleteFeedResource(feedResource: feedResource)
        self.feedResources = DataBaseManager.sharedInstance.getFeedResource()
    }
}

extension FeedResourceListViewController: FeedResourceTableViewCellDelegate {
    func updateTableView() {
        feedResources = DataBaseManager.sharedInstance.getFeedResource()
    }
}

private extension FeedResourceListViewController {
    func setupNavigationItems() {
        navigationItem.rightBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: .add, target: self, action: #selector(addResource))
    }

    @objc func addResource() {
        let alert = UIAlertController(title: "RSS", message: "Add new RSS", preferredStyle: .alert)

        alert.addTextField { (textField) in
            textField.placeholder = "Name"
        }

        alert.addTextField { (textField) in
            textField.placeholder = "Link"
            textField.text = "http://"
        }

        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
            let nameTextField = alert.textFields![0]
            let linkTextField = alert.textFields![1]

            var name = ""
            var link = ""

            if nameTextField.text?.characters.count == 0 {
                name = "No Name"
            }
            else {
                name = (nameTextField.text)!
            }

            if linkTextField.text?.characters.count == 0 {
                return
            }
            else {
                link = (linkTextField.text)!
            }

            DataBaseManager.sharedInstance.addFeedResource(name: name, link: link)
            self.feedResources = DataBaseManager.sharedInstance.getFeedResource()
        }))

        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action: UIAlertAction!) in
        }))

        self.present(alert, animated: true, completion: nil)
    }
}
