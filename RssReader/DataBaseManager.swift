//
//  DataBaseManager.swift
//  RssReader
//
//  Created by Alexandr Ponomarev on 13/02/2017.
//  Copyright © 2017 Alex. All rights reserved.
//

import Foundation
import RealmSwift

class DataBaseManager {
    static let sharedInstance = DataBaseManager()

    let realmInstance = try! Realm()

    func getFeedResource() -> [FeedResource] {
        var feedResources = [FeedResource]()
        for feedResource in realmInstance.objects(FeedResource.self) {
            feedResources.append(feedResource)
        }

        return feedResources
    }

    func addFeedResource(name: String, link: String) {
        let feedResource = getFeedResource()
        let feedWithMaxID = feedResource.max{ $0.id < $1.id }
        var id = 0
        
        if feedWithMaxID != nil {
            id = (feedWithMaxID?.id)! + 1
        }
        
        let feed = FeedResource.getFeedResourceObject(
            id: id,
            name: name,
            url: link,
            isFavorite: false
        )

        try! realmInstance.write {
            realmInstance.add(feed)
        }
    }

    func editFeedResource(id: Int, name: String, link: String, isFavorite: Bool) {
        let feed = FeedResource.getFeedResourceObject(
            id: id,
            name: name,
            url: link,
            isFavorite: isFavorite
        )

        try! realmInstance.write {
            realmInstance.add(feed, update: true)
        }
    }

    func deleteFeedResource(feedResource: FeedResource) {
        try! realmInstance.write {
            realmInstance.delete(feedResource)
        }
    }

    func addRemoveFavorite(id: Int) {
        let feedResource = getFeedResource()

        let currentResource = feedResource.filter{ $0.id == id }.first

        guard let f = currentResource else {
            return
        }

        let feed = FeedResource.getFeedResourceObject(
            id: f.id,
            name: f.name,
            url: f.url,
            isFavorite: !f.isFavorite
        )

        try! realmInstance.write {
            realmInstance.add(feed, update: true)
        }
    }
}

