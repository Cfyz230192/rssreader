//
//  FeedResource.swift
//  RssReader
//
//  Created by Alexandr Ponomarev on 11/02/2017.
//  Copyright © 2017 Alex. All rights reserved.
//

import Foundation
import RealmSwift

class FeedResource: Object {
    dynamic var id = Int()
    dynamic var name = String()
    dynamic var url = String()
    dynamic var isFavorite = Bool()

    override static func primaryKey() -> String? {
        return "id"
    }

    static func getFeedResourceObject(id: Int, name: String, url:String, isFavorite: Bool) -> FeedResource {
        let feedResource = FeedResource()
        feedResource.id = id
        feedResource.name = name
        feedResource.url = url
        feedResource.isFavorite = isFavorite
        return feedResource
    }
}
