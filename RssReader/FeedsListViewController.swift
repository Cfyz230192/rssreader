//
//  FeedsListViewController.swift
//  RssReader
//
//  Created by Alexandr Ponomarev on 11/02/2017.
//  Copyright © 2017 Alex. All rights reserved.
//

import UIKit
import RealmSwift

class FeedsListViewController: BaseViewController {
    
    @IBOutlet private weak var tableView: UITableView!

    var feeds = [[FeedResource : [Feed]]]() {
        didSet {
            tableView.reloadData()
        }
    }

    var favoriteFeeds = [[FeedResource : [Feed]]]()

    var isFavoriteShow = false {
        didSet {
            tableView.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "RSS"

        tableView.registerReusable(reusableClass: FeedTableViewCell.self)

        setupNavigationItems()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let feedResource = DataBaseManager.sharedInstance.getFeedResource()
        feeds.removeAll()
        
        NetworkManager.sharedInstance.getFeedsForURL(feedResource: feedResource) { (feeds) in
            self.feeds.append(feeds)
        }
    }
}

extension FeedsListViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        if isFavoriteShow {
            return favoriteFeeds.count
        }

        return feeds.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFavoriteShow {
            let dict = favoriteFeeds[section]
            let feedsArray = dict[dict.keys.first!]
            return (feedsArray?.count)!
        }

        let dict = feeds[section]
        let feedsArray = dict[dict.keys.first!]
        return (feedsArray?.count)!
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FeedTableViewCell.reuseIdentifier, for: indexPath)

        let dict: [FeedResource : [Feed]]

        if isFavoriteShow {
            dict = favoriteFeeds[indexPath.section]
        }
        else {
            dict = feeds[indexPath.section]
        }

        let feedsArray = dict[dict.keys.first!]

        (cell as! FeedTableViewCell).titleLabel.text = feedsArray?[indexPath.row].title
        (cell as! FeedTableViewCell).descriptionLabel.text = feedsArray?[indexPath.row].description

        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if isFavoriteShow {
            let dict = favoriteFeeds[section]
            return dict.keys.first?.name
        }

        let dict = feeds[section]
        return dict.keys.first?.name
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller = FeedViewController()

        let dict: [FeedResource : [Feed]]

        if isFavoriteShow {
            dict = favoriteFeeds[indexPath.section]
        }
        else {
            dict = feeds[indexPath.section]
        }

        let feedsArray = dict[dict.keys.first!]
        controller.feed = feedsArray?[indexPath.row]


        navigationController?.pushViewController(controller, animated: true)
    }
}

private extension FeedsListViewController {
    func setupNavigationItems() {
        navigationItem.rightBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: .organize, target: self, action: #selector(showResourceList))
    }

    @objc func showResourceList() {
        navigationController?.pushViewController(FeedResourceListViewController(), animated: true)
    }

    @IBAction func changeSegmentAction(_ sender: UISegmentedControl) {

        switch sender.selectedSegmentIndex {
        case 0:
            isFavoriteShow = false
        case 1:
            favoriteFeeds = feeds.filter{ $0.keys.first?.isFavorite == true }
            isFavoriteShow = true
        default:
            return
        }
    }
}
