//
//  FeedResourceTableViewCell.swift
//  RssReader
//
//  Created by Alexandr Ponomarev on 13/02/2017.
//  Copyright © 2017 Alex. All rights reserved.
//

import UIKit

protocol FeedResourceTableViewCellDelegate {
    func updateTableView()
}

class FeedResourceTableViewCell: UITableViewCell {

    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var linkLabel: UILabel!
    @IBOutlet private weak var favoriteButton: UIButton!

    var delegate: FeedResourceTableViewCellDelegate?

    var feedResource: FeedResource! {
        didSet {
            nameLabel.text = feedResource.name
            linkLabel.text = feedResource.url

            favoriteButton.isSelected = feedResource.isFavorite
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

private extension FeedResourceTableViewCell {
    @IBAction func favoriteButtonTapped(_ sender: Any) {
        DataBaseManager.sharedInstance.addRemoveFavorite(id: feedResource.id)
        delegate?.updateTableView()
    }
}
