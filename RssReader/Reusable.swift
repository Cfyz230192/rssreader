//
//  Reusable.swift
//  RssReader
//
//  Created by Alexandr Ponomarev on 11/02/2017.
//  Copyright © 2017 Alex. All rights reserved.
//

import UIKit

protocol Reusable {
    static var reuseIdentifier: String { get }
}

extension Reusable {
    static var reuseIdentifier: String {
        return "\(self)"
    }
}

extension UITableViewCell: Reusable {}
