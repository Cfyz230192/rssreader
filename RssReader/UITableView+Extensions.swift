//
//  UITableView+Extensions.swift
//  RssReader
//
//  Created by Alexandr Ponomarev on 11/02/2017.
//  Copyright © 2017 Alex. All rights reserved.
//

import UIKit

extension UITableView {
    func registerReusable<T: Reusable>(reusableClass: T.Type, nib: UINib? = nil) {
        self.register(nib ?? UINib(nibName: T.reuseIdentifier, bundle: nil) ,
                         forCellReuseIdentifier: T.reuseIdentifier)
    }
}
